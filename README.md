# EduID SP login

Login helper to EduID using preinstalled SimpleSAMLphp SP instance for Yii2 applications

Version 1.4 released 2022-03-25

## Prerequisites

- yii2 >= 2.0.13
- php >= 5.6
- SimpleSamlPHP >= 1.14

## Installation

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

To install, either run
```
    composer require uhi67/yii2-eduid "1.*" 
```

or add 

```
"uhi67/yii2-eduid" : "1.*"
```

or clone form github

```
    git clone https://github.com/uhi67/yii2-eduid
```

## Usage

### Settings in config file (web.php)

```
'components' => [
    'saml' => [
        'class' => 'uhi67\eduidsp\Saml',
        'simpleSamlPath' => '/usr/share/simplesamlphp/',
        'authSource' => 'default-sp'
        'idAttribute' => 'eduPersonTargetedID',    ]
]
```

'simpleSamlPath' property is needed only if SimpleSAMLphp is separately installed and not included by composer itself into current project.

### Usage in code

    /**
     * Login action.
     *
     * @return Response
     */
    public function actionLogin() {
    	/** @var Saml $saml */
	    $saml = Yii::$app->saml;
	    if($saml->isAuthenticated() && !Yii::$app->user->isGuest) return $this->goHome();
	    if (!$saml->isAuthenticated()) {
		    if(!$saml->requireAuth()) { // optional params array may be passed
		    	throw new ForbiddenHttpException('Authentication failure');
		    };
	    }

	    /** @var SamlIdentityInterface $identityClass */
	    $identityClass = Yii::$app->user->identityClass;
	    /** @var SamlIdentityInterface|ActiveRecord $appUser -- Locate user record in identity table */
	    $appUser = $identityClass::findIdentityByUid($saml->id);
	    if(!$appUser) {
	        // Optional attribute check if user can be created goes here
	        // Create new user record
		    $appUser = new $identityClass();
	    }
	    $appUser->setSamlAttributes($saml->attributes);
	    $appUser->save();
	    Yii::$app->user->login($appUser);
	    return $this->goHome();
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout() {
	    /** @var Saml $saml */
	    $saml = Yii::$app->saml;
	    Yii::$app->user->logout();
	    if($saml->isAuthenticated()) $saml->logout(); // This call never returns
	    return $this->goHome();
    }

### Usage in views

For normal login button, use `eduid-login` class, for light version use `eduid-login eduid-light` classes.

```
<?php
use uhi67\eduidsp\SamlAsset;

/* @var yii\web\View $this */
SamlAsset::register($this);
?>
<?= Html::a(Yii::t('app', 'Login'), ['saml'], ['class' => 'btn btn-primary eduid-login eduid-light']) ?>
```

### Usage in In yii2/bootstrap nav bar

    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
	echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ...
            Yii::$app->saml->isAuthenticated() ?
	            ['label' => Yii::$app->saml->attributes['displayName'][0], 'url' => ['/site/logout']] :
                ['label' => Yii::t('app', 'Login'), 'url' => ['/site/login'], 'options'=>['class'=>'eduid-login']],
        ],
    ]);
    NavBar::end();
    ?>


## API documentation

### \uhi67\eduidsp\Saml
    - isAuthenticated() -- determines if the user is currently authenticated in SAMl/EduID
    - requireAuth() -- Initializes login on configured authentication source
    - attributes -- all attributes of the logged in user got from IdP
    - logout() -- Forces logout from SAML/EduiID
    - formatAttribute() -- Html formats attribute value set
    - translateAttributeName() -- Translates attribute name to given language
    - scope -- the best scope value determined from csoped attribute values. Waring: the scope is not clearly mapped to the organization 
    
### \uhi67\eduidsp\SamlIdentityInterface
    - setSamlAttributes($attributes)

Changes
-------
### 1.5 (2022-04-12)

- logout parameters
- login parameters doc

### 1.4 (2022-03-25)

- scope property added

### 1.3 (2020-10-20)

- requireAuth() now can accept an optional params array

### 1.2.3

- Using new namespaces if exist

### 1.2.2

- Autoload namespace bug

### 1.2.1

- Composer-builded SimpleSAMLphp compatibility

### 1.2

- Saml::formatAttribute() added
- Saml::translateAttributeName() added

### 1.1.x

- bugfixes

### 1.1

- First release
