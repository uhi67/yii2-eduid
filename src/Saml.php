<?php /** @noinspection PhpUnused */

namespace uhi67\eduidsp;

use Exception;
use SimpleSAML\Auth\Simple;
use SimpleSAML\Configuration;
use SimpleSAML\Session;
use SimpleSAML\XHTML\Template;
use /** @noinspection PhpUndefinedClassInspection */
	SimpleSAML_Auth_Simple;
use SimpleXMLElement;
use yii\base\BaseObject;
use yii\helpers\Html;

/**
 * Class Saml
 *
 * @package uhi67\eduidsp
 * @property-read array $attributes -- All attributes of the logged-in user or NULL
 * @property-read Simple simpleSaml -- The SimpleSAMLphp authenticator instance
 * @property-read string $id -- the value of the configured identity attribute of the logged-in user or NULL
 * @property-read string $idp -- the actual IdP entity which identified the user (if logged in)
 * @property-read string $scope -- the best scope value derived form eduPersonPrincipalName or eduPersonScopedAffiliation; or NULL
 */
class Saml extends BaseObject {
	/** @var string $simpleSamlPath --  Path to external SimpleSAMLphp instance on this server
	 * - must be readable by the application
	 * - must be null if simpleSAMLphp is included by composer
	 */
	public $simpleSamlPath = null;

    /** @var string $authSource -- name of authSource record in simplesamlphp\config\authsources.php file */
    public $authSource = "default-sp";

    /** @var string $idAttribute -- identity attribute of user */
    public $idAttribute;

    /** @var Simple $simpleSaml */
    private $simpleSaml;

	/** @var Template $_template -- cached template for translating attributes */
	private static $_template;

	/**
	 * Magic method for retrieving SAML attribute values
	 *
	 * @param string $attributeName
	 * @param int|null $index -- which one from the value array or (null=) all of them.
	 *
	 * @return string|array|null -- null: attribute is not found
	 */
    public function get($attributeName, $index=null) {
    	if(array_key_exists($attributeName, $attributes = $this->attributes)) {
    		$value = $attributes[$attributeName];
    		return ($index!==null) ? $value[$index] : $value;
		}
		return null;
	}

	/**
	 * Initializes the object.
	 * @throws
	 */
	public function init() {
		parent::init();
		if(!class_exists('\SimpleSAML\Auth\Simple') && !class_exists('\SimpleSAML_Auth_Simple')) {
			if(is_file($this->simpleSamlPath . '/lib/_autoload.php')) {
				/** @noinspection PhpIncludeInspection */
				require_once($this->simpleSamlPath . '/lib/_autoload.php');
			} else {
				/** @noinspection PhpUnhandledExceptionInspection */
				throw new Exception("SimpleSamlPHP cannot be found at path '$this->simpleSamlPath'");
			}
		}
		if(!class_exists('\SimpleSAML\Auth\Simple')) {
			/** @noinspection PhpUndefinedClassInspection */
			$this->simpleSaml = new SimpleSAML_Auth_Simple($this->authSource);
		}
		else {
			$this->simpleSaml = new Simple($this->authSource);
		}
    }

    /**
	 * Returns value of read-only property
     * @return Simple
     */
    public function getSimpleSaml() {
        return $this->simpleSaml;
    }

    /**
     * Returns value of configured id attribute of the user
     * @return string
     * @throws Exception
     */
    public function getId() {
    	if(!isset($this->attributes[$this->idAttribute])) return null;
    	$value = $this->attributes[$this->idAttribute];
		if(is_array($value)) $value = $value[0];
    	if($this->idAttribute == 'eduPersonTargetedID') {
    		$eptid = new SimpleXMLElement($value);
    		$value = $eptid['NameQualifier'] . '!' . $eptid['SPNameQualifier'] . '!' . $eptid;
		}
		return $value;
	}

    /**
     * @return bool
     */
    public function isAuthenticated() {
        if ($this->simpleSaml != null) {
            return $this->simpleSaml->isAuthenticated();
        }
        return false;
    }

    /**
	 * Initiate login.
     * If the user is already authenticated, this function returns immediately.
	 *
	 * Example params:
	 *
	 * - 'saml:idp' => url of idp to use (if authSource does not specify it)
     *  - 'ErrorURL': A URL that should receive errors from the authentication.
     *  - 'KeepPost': If the current request is a POST request, keep the POST data until after the authentication.
     *  - 'ReturnTo': The URL the user should be returned to after authentication.
     *  - 'ReturnCallback': The function we should call after the user has finished authentication.
	 *
	 * @param array $params
     * @return bool
     */
    public function requireAuth($params=[]) {
        if ($this->simpleSaml != null) {
            $this->simpleSaml->requireAuth($params);
			return true;
        }
        return false;
    }

    /**
	 * Returns attributes of the logged-in user in an array.
	 * Returns empty array if user is not authenticated.
	 * Returns false if SimpleSaml is not initialized.
	 *
     * @return array|bool
     */
    public function getAttributes() {
        if ($this->simpleSaml != null) {
            return $this->simpleSaml->getAttributes();
        }
        return false;
    }

	/**
	 * Log the user out.
	 *
	 * This function logs the user out. It will never return. By default, it will cause a redirect to the current page
	 * after logging the user out, but a different URL can be given with the $params parameter.
	 *
	 * Generic parameters are:
	 *  - 'ReturnTo': The URL the user should be returned to after logout.
	 *  - 'ReturnCallback': The function that should be called after logout.
	 *  - 'ReturnStateParam': The parameter we should return the state in when redirecting.
	 *  - 'ReturnStateStage': The stage the state array should be saved with.
	 *
	 * Returns false on error (if SimpleSaml is not initialized)
	 *
	 * @param string|array|null $params Either the URL the user should be redirected to after logging out, or an array
	 * with parameters for the logout. If this parameter is null, we will return to the current page.
	 * @return bool
	 */
	public function logout($params=null) {
		if ($this->simpleSaml != null) {
			$this->simpleSaml->logout($params);
		}
		return false;
	}

	/**
	 * Returns idp entity identifier of the IdP identified the current user
	 *
	 * @return NULL|string
	 */
	function getIdp() {
		$idp = null;
		if(method_exists($this->simpleSaml, 'getAuthData')) {
			$idp = $this->simpleSaml->getAuthData('saml:sp:IdP');
		}
		else if(method_exists('SimpleSAML_Session', 'getInstance')) {
			// SimpleSamlPhp Older than 1.9 version
			/** @var Session $session */
            /** @noinspection PhpUndefinedMethodInspection */
            $session = Session::getInstance();
            /** @noinspection PhpUndefinedMethodInspection */
            $idp = $session->getIdP();
		}
		return $idp;
	}

    /**
     * Returns the scope value of the logged-in user if available
     *
     * Tries `eduPersonPrincipalName`, `eduPersonScopedAffiliation` in this order.
     * If the user not logged in, or none of the attributes above is available, the result is null.
     *
     * @return string|null
     */
    public function getScope() {
        if(!$this->isAuthenticated()) return null;
        if(($eduPersonPrincipalName = $this->get('eduPersonPrincipalName', 0)) && strpos($eduPersonPrincipalName, '@')) return substr(strrchr($eduPersonPrincipalName, '@'),1);
        if($eduPersonScopedAffiliations = $this->get('eduPersonScopedAffiliation')) {
            foreach($eduPersonScopedAffiliations as $eduPersonScopedAffiliation) {
                if(strpos($eduPersonScopedAffiliation, '@')) return substr(strrchr($eduPersonScopedAffiliation, '@'),1);
            }
        }
        return null;
    }

	/**
	 * Translates attribute name
	 *
	 * @param string $attributeName
	 * @param string $la -- ISO 639-1 language or ISO 3166-1 locale
	 *
	 * @return string -- translated name or original if translation is not found
	 * @throws Exception
	 */
	public static function translateAttributeName($attributeName, $la) {
		if(
			(!class_exists('\SimpleSAML\Configuration') && !class_exists('\SimpleSAML_Configuration')) ||
			(!class_exists('\SimpleSAML\XHTML\Template') && !class_exists('\SimpleSAML_XHTML_Template'))
		) return $attributeName;
		if(static::$_template) $t = static::$_template;
		else {
			$globalConfig = Configuration::getInstance();
			$t = (static::$_template = new Template($globalConfig, 'status.php', 'attributes'));
			if(method_exists('\SimpleSAML\Locale\Language', 'setLanguage')) {
				$t->getTranslator()->getLanguage()->setLanguage(substr($la, 0, 2), false);
			}
			else {
                /** @noinspection PhpDeprecationInspection */
                $t->setLanguage(substr($la, 0, 2), false);
			}
		}
		if(method_exists('\SimpleSAML\Locale\Translate', 'getAttributeTranslation')) {
			$translated = $t->getTranslator()->getAttributeTranslation($attributeName);
		}
		else {
            /** @noinspection PhpDeprecationInspection */
            $translated = $t->getAttributeTranslation($attributeName);
		}
		return $translated;
	}

	/**
	 * Html-formats values of attribute, depending on attribute name
	 *
	 * @param string $attributeName
	 * @param string|array $values
	 *
	 * @return string
	 */
	public static function formatAttribute($attributeName, $values) {
		if (!is_array($values)) $values = [$values];
		if (count($values) == 0) return '';
		if (count($values) == 1) return static::formatValue($attributeName, $values[0]);
		return Html::tag('ul', implode('', array_map(function ($v) use($attributeName) {
			return Html::tag('li', static::formatValue($attributeName, $v));
		}, $values)));
	}

	/**
	 * Html-formats a single value of attribute, depending on attribute name
	 *
	 * @param string $attributeName
	 * @param string $value
	 *
	 * @return string
	 */
	public static function formatValue($attributeName, $value) {
		if($attributeName == 'jpegPhoto') return Html::tag('img', '', ['src' => 'data:image/jpeg;base64,'. $value]);
		if(is_array($value)) return '['. implode(', ', array_map(function($k,$v) use($attributeName) {
				return (is_integer($k) ? '' : $k . ': ') . self::formatValue($attributeName.'.'.$k, $v);
			}, array_keys($value), array_values($value))). ']';
		return $value;
	}
}
