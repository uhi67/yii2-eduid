<?php /** @noinspection PhpUnused */

namespace uhi67\eduidsp;

use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

interface SamlIdentityInterface extends IdentityInterface {
	/**
	 * Must set user's attributes from SAML attributes.
	 *
	 * $attributes is an array containing [attributename => [value, ...], ...]
	 *
	 * @param array $attributes
	 * @return void
	 */
	public function setSamlAttributes($attributes);

	/**
	 * Must return the identity record by configured SAML identity attribute
	 *
	 * @param string $uid
	 * @return SamlIdentityInterface|ActiveRecord
	 */
	public static function findIdentityByUid($uid);
}
