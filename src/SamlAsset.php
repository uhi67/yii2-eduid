<?php /** @noinspection PhpUnused */

namespace uhi67\eduidsp;

use yii\web\AssetBundle;

class SamlAsset extends AssetBundle {
    public $sourcePath = '@vendor/uhi67/yii2-eduid/src/assets';

    public $css = [
        'saml.css',
    ];

}
